# Laravel DDD project structure generator



## What is it for?

This package generates project foundation from single config

Result of generation is:
- Contract for every entity
- Migrations for every entity
- Models for every entity
- GET, CREATE, UPDATE request with validation
- Controllers for every entity
- Base repository with all CRUD methods, configured by QueryFilters with the help of Pipeline
- Repository extended from Base repository for every entity
- Base service with all CRUD methods
- Service extended from Base service for every entity
- Seeders and factories
- CRUD routes for every entity
- CRUD admin pages and base structure for Nuxt 

## How to use?

- Create new Laravel project
- Complete all necessary configuration, like filling .env
- composer require japananimetime/template
- Create config/entities.php
- Describe your entities there (Example is below)
- php artisan make:entity
- Enjoy!

## Config example

```
<?php

return [
    'user'      => [
        'fields'      => [
            'name'              => [
                'type'       => 'string',
                'nullable'   => false,
                'unique'     => false,
                'fillable'   => true,
                'faker'      => [true, 'name()'],
                'validation' => [
                    'create' => [
                        'string'   => 'true',
                        'required' => 'true',
                    ],
                    'update' => [
                        'string' => 'true',
                    ],
                    'get'    => [

                    ],
                ],
            ],
            'email'             => [
                'type'       => 'string',
                'nullable'   => false,
                'unique'     => true,
                'fillable'   => true,
                'faker'      => [true, 'email()'],
                'validation' => [
                    'create' => [
                        'string'   => 'true',
                        'email'    => 'email:rfc,dns',
                        'required' => 'true',
                    ],
                    'update' => [
                        'string' => 'true',
                        'email'  => 'email:rfc,dns',
                    ],
                    'get'    => [

                    ],
                ],
            ],
            'email_verified_at' => [
                'type'       => 'string',
                'nullable'   => true,
                'unique'     => false,
                'fillable'   => true,
                'faker'      => [true, 'unixTime()'],
                'validation' => [
                    'create' => [

                    ],
                    'update' => [

                    ],
                    'get'    => [

                    ],
                ],
            ],
            'phone'             => [
                'type'       => 'string',
                'nullable'   => false,
                'unique'     => true,
                'fillable'   => true,
                'faker'      => [true, 'phoneNumber'],
                'validation' => [
                    'create' => [
                        'string'   => 'true',
                        'required' => 'true',
                    ],
                    'update' => [
                        'string' => 'true',
                    ],
                    'get'    => [

                    ],
                ],
            ],
            'password'          => [
                'type'       => 'string',
                'nullable'   => true,
                'unique'     => false,
                'fillable'   => true,
                'faker'      => [false, '\\Illuminate\\Support\\Facades\\Hash::make(\'123456\')'],
                'validation' => [
                    'create' => [
                        'string'   => 'true',
                        'password' => ['required', 'confirmed', 'Illuminate\Validation\Rules\Password::min(6)'],
                        'required' => 'true',
                    ],
                    'update' => [
                        'string'   => 'true',
                        'password' => ['required', 'confirmed', 'Illuminate\Validation\Rules\Password::min(6)'],
                    ],
                    'get'    => [

                    ],
                ],
            ],
            'remember_token'    => [
                'type'       => 'string',
                'nullable'   => true,
                'unique'     => false,
                'fillable'   => false,
                'faker'      => [false, 'null'],
                'validation' => [
                    'create' => [

                    ],
                    'update' => [

                    ],
                    'get'    => [

                    ],
                ],
            ],
            'subscription'      => [
                'type'       => 'bool',
                'nullable'   => false,
                'unique'     => false,
                'fillable'   => true,
                'faker'      => [true, '1'],
                'validation' => [
                    'create' => [
                        'required' => 'true'
                    ],
                    'update' => [
                        'bool' => 'true'
                    ],
                    'get'    => [

                    ]
                ]
            ],
            'city'              => [
                    'type'       => 'string',
                    'nullable'   => false,
                    'unique'     => false,
                    'fillable'   => true,
                    'faker'      => [true, 'city()'],
                    'validation' => [
                        'create' => [
                            'string'   => 'true',
                            'required' => 'true',
                        ],
                        'update' => [
                            'string' => 'true',
                        ],
                        'get'    => [

                        ],
                    ],
                ],
            'language'              => [
                'type'       => 'string',
                'nullable'   => false,
                'unique'     => false,
                'fillable'   => true,
                'faker'      => [true, 'language()'],
                'validation' => [
                    'create' => [
                        'string'   => 'true',
                        'required' => 'true',
                    ],
                    'update' => [
                        'string' => 'true',
                    ],
                    'get'    => [

                    ],
                ],
            ],

        ],
        'requests'    => [
            'create' => [
                'auth' => true,
            ],
            'update' => [
                'auth' => true,
            ],
            'get'    => [
                'auth' => false,
            ],
        ],
        'softDeletes' => true,
        'pageLength'  => 16,
    ],
    'category'  => [
        'fields' => [
            'title' => [
                'type'       => 'string',
                'nullable'   => false,
                'unique'     => false,
                'fillable'   => true,
                'faker'      => [true, 'title()'],
                'validation' => [
                    'create' => [
                        'string'   => 'true',
                        'required' => 'true',
                    ],
                    'update' => [
                        'string' => 'true',
                    ],
                    'get'    => [

                    ],
                ],
            ],
            'image' => [
                'type'       => 'string',
                'nullable'   => false,
                'unique'     => false,
                'fillable'   => true,
                'faker'      => [true, 'imageUrl(640, 480)'],
                'validation' => [
                    'create' => [
                        'string'   => 'true',
                        'required' => 'true',
                    ],
                    'update' => [
                        'string' => 'true',
                    ],
                    'get'    => [

                    ],
                ],
            ],
        ],
        'requests'    => [
            'create' => [
                'auth' => true,
            ],
            'update' => [
                'auth' => true,
            ],
            'get'    => [
                'auth' => false,
            ],
        ],
        'softDeletes' => true,
        'pageLength'  => 16,
    ],
    'product'   => [
        'fields' => [
            'category_id' => [
                'type'       => 'unsignedBigInteger',
                'nullable'   => true,
                'unique'     => false,
                'fillable'   => true,
                'faker'      => [false, '\\App\\Models\\Category::inRandomOrder()->first() ? \\App\\Models\\Category::inRandomOrder()->first()->id : null'],
                'validation' => [
                    'create' => [
                        'integer'   => 'true',
                    ],
                    'update' => [
                        'integer' => 'true',
                    ],
                    'get'    => [

                    ],
                ],
            ],
            'title' => [
                'type'       => 'string',
                'nullable'   => false,
                'unique'     => false,
                'fillable'   => true,
                'faker'      => [true, 'title()'],
                'validation' => [
                    'create' => [
                        'string'   => 'true',
                        'required' => 'true',
                    ],
                    'update' => [
                        'string' => 'true',
                    ],
                    'get'    => [

                    ],
                ],
            ],
            'image' => [
                'type'       => 'string',
                'nullable'   => false,
                'unique'     => false,
                'fillable'   => true,
                'faker'      => [true, 'imageUrl(640, 480)'],
                'validation' => [
                    'create' => [
                        'string'   => 'true',
                        'required' => 'true',
                    ],
                    'update' => [
                        'string' => 'true',
                    ],
                    'get'    => [

                    ],
                ],
            ],
            'description'  => [
                'type'       => 'text',
                'nullable'   => false,
                'unique'     => false,
                'fillable'   => true,
                'faker'      => [true, 'text'],
                'validation' => [
                    'create' => [
                        'string'   => 'true',
                        'required' => 'true',
                    ],
                    'update' => [
                        'string' => 'true',
                    ],
                    'get'    => [

                    ],
                ],
            ],
            'price' => [
                'type'      =>  'integer',
                'nullable'  =>  false,
                'unique'     => false,
                'fillable'   => true,
                'faker'      => [false, 'integer'],
                'validation' => [
                    'create' => [
                        'string'   => 'true',
                        'required' => 'true',
                    ],
                    'update' => [
                        'string' => 'true',
                    ],
                    'get'    => [

                    ],
                ],
            ]
        ],
        'requests'    => [
            'create' => [
                'auth' => true,
            ],
            'update' => [
                'auth' => true,
            ],
            'get'    => [
                'auth' => false,
            ],
        ],
        'softDeletes' => true,
        'pageLength'  => 16,
    ],
];

```
## Usage of QueryFilter with Pipeline

### QueryFilter
```
<?php


namespace App\v2\QueryFilters\Product;


use App\v2\Contracts\ProductContract;

class Hidden
{
    /**
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param                                       $next
     *
     * @return mixed
     */
    public function handle(\Illuminate\Database\Eloquent\Builder $query, $next)
    {
        if(request()->has('hidden')){
            $query->where(ProductContract::HIDDEN, true);
        }

        return $next($query);
    }
}
```
### Controller
```
<?php

namespace App\Http\Controllers\API\V1;

use App\Http\Controllers\Controller;
use App\QueryFilters\Product\Hidden;
use App\Http\Requests\GetProductRequest;
use App\Services\ProductService;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    private ProductService $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }

    public function universal(Request $request)
    {
        $result = $this->productService->paginate(
            [
                Hidden::class,
            ]
        );

        return response($result['message'], $result['status']);
    }
```

## Base CRUD admin on Nuxt

After generation you get folder admin in public

Create new nuxt project somewhere and copy folders from admin to nuxt project

##TODO:

- Fix formatting in generated files
- Make admin pages more usable, at least ad support for different fields, for example image plugin instead of only text fields
- Complete graphic interface for config creation
