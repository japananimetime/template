@php use Illuminate\Support\Str; @endphp
namespace {{ $namespace }};

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\{{ $model }}Service;
use App\Http\Requests\Get{{ $model }}Request;
use App\Http\Requests\Create{{ $model }}Request;
use App\Http\Requests\Update{{ $model }}Request;

class {{ $model }}Controller extends Controller
{
    public {{ $model }}Service ${{ Str::camel($model) }}Service;

    public function __construct({{ $model }}Service ${{ Str::camel($model) }}Service)
    {
        $this->{{ Str::camel($model) }}Service = ${{ Str::camel($model) }}Service;
    }

    /**
    * Display a listing of the resource.
    *
    * @param \App\Http\Requests\Get{{ $model }}Request $request
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Get{{ $model }}Request $request): \Illuminate\Http\Response
    {
        $result = $this->{{ Str::camel($model) }}Service->all();
        return response($result->data, $result->status);
    }

    /**
    * Display paginated listing of the resource.
    *
    * @param \App\Http\Requests\Get{{ $model }}Request $request
    *
    * @return  \Illuminate\Http\Response
    */
    public function paginate(Get{{ $model }}Request $request): \Illuminate\Http\Response
    {
        $result = $this->{{ Str::camel($model) }}Service->paginate();
        return response($result->data, $result->status);
    }

    /**
    * Store a newly created resource in storage.
    *
    * @param \App\Http\Requests\Create{{ $model }}Request $request
    *
    * @return  \Illuminate\Http\Response
    */
    public function store(Create{{ $model }}Request $request): \Illuminate\Http\Response
    {
        $result = $this->{{ Str::camel($model) }}Service->create($request->validated());
        return response($result->data, $result->status);
    }

    /**
    * Display the specified resource.
    *
    * @param \App\Http\Requests\Get{{ $model }}Request $request
    * @param int                               $id
    *
    * @return  \Illuminate\Http\Response
    */
    public function show(Get{{ $model }}Request $request, int $id): \Illuminate\Http\Response
    {
        $result = $this->{{ Str::camel($model) }}Service->show($id);
        return response($result->data, $result->status);
    }

    /**
    * Update the specified resource in storage.
    *
    * @param \App\Http\Requests\Update{{ $model }}Request $request
    * @param int                                  $id
    *
    * @return  \Illuminate\Http\Response
    */
    public function update(Update{{ $model }}Request $request, int $id): \Illuminate\Http\Response
    {
        $result = $this->{{ Str::camel($model) }}Service->update($request->validated(), $id);
        return response($result->data, $result->status);
    }

    /**
    * Remove the specified resource from storage.
    *
    * @param int $id
    *
    * @return  \Illuminate\Http\Response
    */
    public function destroy(int $id): \Illuminate\Http\Response
    {
        $result = $this->{{ Str::camel($model) }}Service->delete($id);
        return response($result->data, $result->status);
    }
}
