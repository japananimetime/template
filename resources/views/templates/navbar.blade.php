export const state = () => ({
    items: [
        @foreach($entities as $item => $properties)
        {
            icon: '{{ $properties['admin']['icon'] }}',
            title: '{{ $properties['admin']['title'] }}',
            to: '/{{ \Illuminate\Support\Str::plural($item) }}',
            roles: [{{ implode(', ', $properties['admin']['roles']) }}],
        },
        @endforeach
    ]
})
