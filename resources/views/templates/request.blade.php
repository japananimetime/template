

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Domain\Contracts\{{ $model }}Contract;
use Illuminate\Support\Facades\Auth;

class {{ $type }}{{ $model }}Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return {{ $params['auth'] ? 'Auth::check();' : 'true;' }}
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
        @foreach($fields as $field => $value)
    {{ $model }}Contract::{{ \Illuminate\Support\Str::upper(\Illuminate\Support\Str::snake($field)) }} => [
        @foreach($value['validation'][\Illuminate\Support\Str::lower($type)] as $rule => $rule_val)
        '{{ $rule }}' => @if(!is_array($rule_val)) '{{ $rule_val }}', @endif @if(is_array($rule_val)) [
            @foreach($rule_val as $rule_val_val)
        '{{ $rule_val_val }}',
            @endforeach
    ],@endif

        @endforeach
    ],
        @endforeach];
    }
}
