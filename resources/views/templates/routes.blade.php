

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\V1\{{ Str::studly($entity) }}Controller;

/*
|--------------------------------------------------------------------------
| Routes for {{ Str::plural($entity) }}
|--------------------------------------------------------------------------
|
| These routes were generated for group {{ $entity }}
| All of them have prefix /api/v1/{{ Str::plural(Str::lower(Str::kebab($entity))) }}
|
*/

Route::get('{{ Str::plural(Str::lower(Str::kebab($entity))) }}/all', [{{ Str::studly($entity) }}Controller::class, 'paginate']);
Route::get('{{ Str::plural(Str::lower(Str::kebab($entity))) }}', [{{ Str::studly($entity) }}Controller::class, 'index']);
Route::get('{{ Str::plural(Str::lower(Str::kebab($entity))) }}/{id}', [{{ Str::studly($entity) }}Controller::class, 'show']);
Route::post('{{ Str::plural(Str::lower(Str::kebab($entity))) }}', [{{ Str::studly($entity) }}Controller::class, 'store']);
Route::post('{{ Str::plural(Str::lower(Str::kebab($entity))) }}/{id}', [{{ Str::studly($entity) }}Controller::class, 'update']);
Route::delete('{{ Str::plural(Str::lower(Str::kebab($entity))) }}/{id}', [{{ Str::studly($entity) }}Controller::class, 'destroy']);

Route::prefix('admin')->group(
    function () {
        Route::get('{{ Str::plural(Str::lower(Str::kebab($entity))) }}/all', [{{ Str::studly($entity) }}Controller::class, 'paginate']);
        Route::get('{{ Str::plural(Str::lower(Str::kebab($entity))) }}', [{{ Str::studly($entity) }}Controller::class, 'index']);
        Route::get('{{ Str::plural(Str::lower(Str::kebab($entity))) }}/{id}', [{{ Str::studly($entity) }}Controller::class, 'show']);
        Route::post('{{ Str::plural(Str::lower(Str::kebab($entity))) }}', [{{ Str::studly($entity) }}Controller::class, 'store']);
        Route::post('{{ Str::plural(Str::lower(Str::kebab($entity))) }}/{id}', [{{ Str::studly($entity) }}Controller::class, 'update']);
        Route::delete('{{ Str::plural(Str::lower(Str::kebab($entity))) }}/{id}', [{{ Str::studly($entity) }}Controller::class, 'destroy']);
    }
);
