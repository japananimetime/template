

namespace {{ $namespace }};

interface {{ $model }}Contract
{

    public const TABLE   = '{{ \Illuminate\Support\Str::plural(\Illuminate\Support\Str::lower($model)) }}';

@foreach($fields as $field)
    public const {{ \Illuminate\Support\Str::upper(\Illuminate\Support\Str::snake($field)) }} = '{{ $field }}';
@endforeach

    //TODO Add fillables to contract instead of model
}
