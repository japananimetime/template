

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    @foreach($entities as $name => $entity)
        \App\Models\{{ \Illuminate\Support\Str::studly($name) }}::factory(10)->create();
    @endforeach
    }
}
