

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Domain\Contracts\{{ $model }}Contract;

class Create{{ \Illuminate\Support\Str::plural($model)  }}Table extends Migration
{
    /**
    * Run the migrations.
    *
    * @return void
    */
    public function up()
    {
        Schema::create({{ $model }}Contract::TABLE, function (Blueprint $table) {
            $table->id();
        @foreach($fields as $fieldname => $field)
$table
            ->{!! $field['type'] !!}({{ $model }}Contract::{{ \Illuminate\Support\Str::upper(\Illuminate\Support\Str::snake($fieldname)) }})
        @if($field['nullable'])
            ->nullable()
        @endif
        @if($field['unique'])
            ->unique()
        @endif
        ;
        @endforeach
        @if($softDeletes)
            $table->softDeletes();
        @endif
            $table->timestamps();
        });
    }

    /**
    * Reverse the migrations.
    *
    * @return void
    */
    public function down()
    {
        Schema::dropIfExists({{ $model }}Contract::TABLE);
    }
}
