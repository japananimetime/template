<template>
    <GenericTable
        :searchable="true"
        :headers="headers"
        :items="data"
        :changeable="true"
        :loading="loading"
        :total="total"
        createlink="/{{ \Illuminate\Support\Str::plural($name) }}/create"
        title="{{ $entity['admin']['title'] }}"
        @get-data="getDataFromApi"
        @change="changeItem"
    />
</template>

<script>
import GenericTable from "@/components/GenericTable";

export default {
    name:       "{{ \Illuminate\Support\Str::plural($name) }}List",
    components: {
        GenericTable
    },

    data() {
        return {
            data:     [],
            loading:  false,
            from:     0,
            to:       0,
            total:    0,
            selected: [],
            headers:  [
@foreach($entity['fields'] as $field => $properties)
                {text: "{{ $properties['label'] }}", value: "{{ $field }}"},
@endforeach
            ],
        }
    },

    methods: {
        getDataFromApi(params) {
            this.loading = true
            this.$axios.$get('/admin/{{ \Illuminate\Support\Str::plural($name) }}', {
                params
            })
                .then((response) => {
                    this.data    = response.data
                    this.from    = response.from
                    this.to      = response.to
                    this.total   = response.total
                    this.loading = false
                })
        },

        changeItem(e) {
            this.$router.push('/{{ \Illuminate\Support\Str::plural($name) }}/' + e.id)
        },
    }
}
</script>

