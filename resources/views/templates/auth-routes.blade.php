

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Japananimetime\Template\Controllers\AuthController;

/*
|--------------------------------------------------------------------------
| Routes for admin auth
|--------------------------------------------------------------------------
|
| These routes were generated for admin auth
|
*/

Route::prefix('v1')
     ->name('v1.')
     ->group(function () {
         Route
             ::middleware(['auth:sanctum'])
             ->prefix('auth')
             ->group(function () {
                 Route::post('login', [AuthController::class, 'login'])
                      ->withoutMiddleware(['auth:sanctum'])
                 ;
                 Route::post('logout', [AuthController::class, 'logout']);
                 Route::post('refresh', [AuthController::class, 'refresh'])
                      ->withoutMiddleware(['auth:sanctum'])
                 ;
                 Route::get('user', [AuthController::class, 'user']);
             })
         ;
     });
