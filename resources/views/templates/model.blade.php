

namespace {{ $namespace }};

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Domain\Contracts\{{ $model }}Contract;
@if($softDeletes)use Illuminate\Database\Eloquent\SoftDeletes;@endif

/**
 * Class {{ $model }}
 * @package App\Models
 *
 * @property int $id
@foreach($fields as $field => $properties)
 * @property {{ $properties['type'] }} ${{ $field }}
@endforeach
 */
class {{ $model }} extends Model
{
    use HasFactory{{ $softDeletes ? ', SoftDeletes' : '' }};

    protected $fillable = [
    @foreach($fillables as $field)
        {{ $model }}Contract::{{ \Illuminate\Support\Str::upper(\Illuminate\Support\Str::snake($field)) }},
    @endforeach
];

    protected $perPage = {{ $pageLength }};

    protected $table = {{ $model }}Contract::TABLE;

}
