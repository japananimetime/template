

namespace {{ $namespace }};

use App\Domain\Contracts\{{ $model }}Contract;
use App\Domain\Repositories\{{ $model }}Repository;
use Symfony\Component\HttpFoundation\Response;
use Japananimetime\Template\ApiResponse;

/**
 * Class {{ $model }}Service
 * @package App\Services
 */
class {{ $model }}Service
{
    protected {{ $model }}Repository ${{ Str::camel($model) }}Repository;

    /**
    * {{ $model }}Service constructor.
    */
    public function __construct({{ $model }}Repository ${{ Str::camel($model) }}Repository) {
            $this->{{ Str::camel($model) }}Repository = ${{ Str::camel($model) }}Repository;
    }

    /**
    * @param array $filters
    *
    * @return \Japananimetime\Template\ApiResponse
    */
    public function all(array $filters = []): ApiResponse
    {
        $result = $this->{{ Str::camel($model) }}Repository->all($filters);

        return new ApiResponse(Response::HTTP_OK, 'Records', $result);
    }

    /**
    * @param array $filters
    *
    * @return \Japananimetime\Template\ApiResponse
    */
    public function paginate(array $filters = []): ApiResponse
    {
        $result = $this->{{ Str::camel($model) }}Repository->paginate($filters);

        return new ApiResponse(Response::HTTP_OK, 'Records', $result);
    }

    /**
    *
    * @param $id
    *
    * @return \Japananimetime\Template\ApiResponse
    *
    */
    public function show($id): ApiResponse
    {
        $result = $this->{{ Str::camel($model) }}Repository->show($id);

        return new ApiResponse(Response::HTTP_OK, 'Record #' . $id, $result);
    }

    /**
    *
    * @param $data
    *
    * @return \Japananimetime\Template\ApiResponse
    *
    */
    public function create($data): ApiResponse
    {
        try {
            DB::beginTransaction();

            $result = $this->{{ Str::camel($model) }}Repository->create($data);

            DB::commit();

            return new ApiResponse(Response::HTTP_OK, 'Created successfully', $result);
        } catch (Exception $exception) {
            DB::rollBack();

            Log::error('Error creating record: ' . $exception->getMessage(), [
                'data' => $data,
            ]);

            return new ApiResponse(Response::HTTP_SERVICE_UNAVAILABLE, 'Creation failed', [
                'detail'  => '',
                'message' => $exception->getMessage(),
            ],);
        }
    }

    /**
    * @param array $data
    * @param       $id
    *
    * @return \Japananimetime\Template\ApiResponse
    */
    public function update(array $data, $id): ApiResponse
    {
        try {
            DB::beginTransaction();

            $result = $this->{{ Str::camel($model) }}Repository->update($data, $id);

            DB::commit();

            return new ApiResponse(Response::HTTP_OK, 'Updated successfully', $result);
        } catch (Exception $exception) {
            DB::rollBack();

            Log::error('Error updating record: ' . $exception->getMessage(), [
            'data' => $data,
            ]);

            return new ApiResponse(Response::HTTP_SERVICE_UNAVAILABLE, 'Update failed', [
                'detail'  => '',
                'message' => $exception->getMessage(),
            ],);
        }
    }

    /**
    *
    * @param
    *
    * @return \Japananimetime\Template\ApiResponse
    *
    */
    public function delete($id): ApiResponse
    {
        $result = $this->{{ Str::camel($model) }}Repository->delete($id);

        return new ApiResponse(Response::HTTP_OK, 'Deleted successfully');
    }
}
