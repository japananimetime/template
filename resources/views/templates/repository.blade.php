

namespace {{ $namespace }};

use Japananimetime\Template\BaseRepository;
use App\Models\{{ $model }};

/**
 * Class {{ $model }}Repository
 * @package App\Repositories
 *
 * @php echo '@method' @endphp \App\Models\{{ $model }} create(array $data)
 * @php echo '@method' @endphp \App\Models\{{ $model }} update(array $data, int $id)
 * @php echo '@method' @endphp \App\Models\{{ $model }} delete(int $id)
 * @php echo '@method' @endphp \App\Models\{{ $model }} show(int $id, array $columns = ['*'])
 */
class {{ $model }}Repository extends BaseRepository
{

    public function model(): {{ $model }}
    {
        return new {{ $model }}();
    }
}
