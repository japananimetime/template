

namespace Database\Factories;

use App\Models\{{ $model  }};
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Domain\Contracts\{{ $model }}Contract;

class {{ $model  }}Factory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = {{ $model  }}::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
    @foreach($fields as $name => $field)
        {{ $model }}Contract::{{ \Illuminate\Support\Str::upper(\Illuminate\Support\Str::snake($name)) }} => @if($field['faker'][0]) $this->faker->{!! $field['faker'][1] !!}@else {!! $field['faker'][1] !!}@endif,
    @endforeach
    ];
    }
}
