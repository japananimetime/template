<?php

namespace Japananimetime\Template\Controllers;

use App\Domain\Contracts\UserContract;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Symfony\Component\HttpFoundation\Response;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $user = User
            ::query()
            ->where(UserContract::EMAIL, $request->login)
            ->first();

        if (empty($user)) {
            return response([], Response::HTTP_UNAUTHORIZED);
        }

        if (Hash::check($request->password, $user->{UserContract::PASSWORD})) {
            return response(
                [
                    'access_token' => $user->createToken('')->plainTextToken,
                ], Response::HTTP_OK
            );
        } else {
            return response([], Response::HTTP_UNAUTHORIZED);
        }
    }

    public function logout(Request $request)
    {
        $request->user()->currentAccessToken()->delete();
    }

    public function refresh()
    {

    }

    public function user(Request $request)
    {
        $user = $request->user();

        return response(['user' => $user], 200);
    }
}
