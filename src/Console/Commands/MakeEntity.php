<?php

namespace Japananimetime\Template\Console\Commands;

use Japananimetime\Template\TemplateService;
use Illuminate\Console\Command;
use Illuminate\Support\Str;

class MakeEntity extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:entity';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create migration, service, requests, controllers, etc for entity';

    protected TemplateService $templateService;

    /**
     * Create a new command instance.
     *
     * @param \Japananimetime\Template\TemplateService $templateService
     */
    public function __construct(TemplateService $templateService)
    {
        parent::__construct();

        $this->templateService = $templateService;
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        foreach (config('entities') as $name => $entity) {
            $modelName = Str::studly($name);

            $fields = collect($entity['fields'])->keys();
            $fillables = collect($entity['fields'])->where('fillable', true);

            $this->templateService->createContract($modelName, $fields, $fillables);
            $this->info('Contract created: ' . $modelName);

            $this->templateService->createModel($modelName, $fillables->keys(), $entity['softDeletes'], $entity['pageLength'], $entity['fields']);
            $this->info('Model created: ' . $modelName);

            $this->templateService->createMigration($modelName, $entity['fields'], $entity['softDeletes']);
            $this->info('Migration created: ' . $modelName);

            $this->templateService->createFactory($modelName, $entity['fields']);
            $this->info('Factory created: ' . $modelName);

            $this->templateService->createAdminPages($modelName, $entity);
            $this->info('Admin page created: ' . $modelName);

            $this->templateService->createCreateRequest($modelName, $fillables, $entity['requests']['create']);
            $this->info('Create request created: ' . $modelName);

            $this->templateService->createUpdateRequest($modelName, $fillables, $entity['requests']['update']);
            $this->info('Update request created: ' . $modelName);

            $this->templateService->createGetRequest($modelName, $fillables, $entity['requests']['get']);
            $this->info('Get request created: ' . $modelName);

            $this->templateService->createController($modelName);
            $this->info('Controller created: ' . $modelName);

            $this->templateService->createService($modelName);
            $this->info('Service created: ' . $modelName);

            $this->templateService->createRepository($modelName);
            $this->info('Repository created: ' . $modelName);

            $this->templateService->createRoutes($modelName);
            $this->info('Routes created: ' . $modelName);

        }

        $this->templateService->createNuxtConfig();
        $this->info('Nuxt config created: ' . $modelName);

        $this->templateService->createItemsState(config('entities'));
        $this->info('Navbar items created: ' . $modelName);

        $this->templateService->createRouteServiceProvider(config('entities'));
        $this->info('RouteServiceProvider created: ' . $modelName);

        $this->templateService->createAdminAuthRoutes();
        $this->info('AdminAuthRoutes created: ' . $modelName);

        $this->templateService->createDatabaseSeeder(config('entities'));
        $this->info('Seeder created: ' . $modelName);

        return 0;
    }
}
