<?php

namespace Japananimetime\Template;

class ApiResponse
{
    public int $status;
    public array $data;

    public function __construct(int $status, string $message = '', $data = null)
    {
        $this->status = $status;
        $this->data = [
            'message' => $message,
            'data' => $data,
        ];
    }
}
