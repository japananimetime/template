<?php


namespace Japananimetime\Template;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Pipeline\Pipeline;
use Spatie\QueryBuilder\QueryBuilder;

abstract class BaseRepository
{
    protected Model $model;

    abstract public function model();

    public function __construct()
    {
        $this->model = $this->model();
    }

    /**
     * @param array $pipes
     * @param array $fields
     * @param array $filters
     * @param array $includes
     * @param array $sorts
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function all(array $pipes = [], array $fields = [], array $filters = [], array $includes = [], array $sorts = []): \Illuminate\Database\Eloquent\Collection
    {
        $result = $this->createReadBuilder($pipes, $fields, $filters, $includes, $sorts);

        return $result->get();
    }

    /**
     * @param array $pipes
     * @param array $fields
     * @param array $filters
     * @param array $includes
     * @param array $sorts
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function paginate(array $pipes = [], array $fields = [], array $filters = [], array $includes = [], array $sorts = []): \Illuminate\Contracts\Pagination\LengthAwarePaginator
    {
        $result = $this->createReadBuilder($pipes, $fields, $filters, $includes, $sorts);

        return $result->paginate(request()->get('limit'));
    }

    /**
     * @param $id
     *
     * @return Model|null
     */
    public function show($id): ?Model
    {
        return $this
            ->model
            ->query()
            ->find($id)
        ;
    }

    /**
     * @param $data
     *
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create($data): Model
    {
        return $this
            ->model
            ->query()
            ->create($data)
        ;
    }

    /**
     * @param array $data
     * @param       $id
     *
     * @return int
     */
    public function update(array $data, $id): int
    {
        return $this
            ->model
            ->query()
            ->findOrFail($id)
            ->update($data)
        ;
    }

    /**
     * @param $id
     *
     * @return mixed
     */
    public function delete($id)
    {
        return $this
            ->model
            ->query()
            ->findOrFail($id)
            ->delete()
        ;
    }

    /**
     * @param array $pipes
     *
     * @param mixed $fields
     * @param mixed $filters
     * @param mixed $includes
     * @param mixed $sorts
     *
     * @return mixed
     */
    public function createReadBuilder(array $pipes, array $fields, array $filters, array $includes, array $sorts)
    {
        $result = QueryBuilder::for($this->model::class);

        if (!empty($fields)) {
            $result = $result->allowedFields($fields);
        }
        if (!empty($filters)) {
            $result = $result->allowedFilters($filters);
        }
        if (!empty($includes)) {
            $result = $result->allowedIncludes($includes);
        }
        if (!empty($sorts)) {
            $result = $result->allowedSorts($sorts);
        }

        return app(Pipeline::class)
            ->send($result)
            ->through(
                $pipes
            )
            ->thenReturn()
        ;
    }
}
