<?php


namespace Japananimetime\Template;


use Carbon\Carbon;
use Illuminate\Support\Str;

class TemplateService
{
    public function createContract($model, $fields, $fillables)
    {
        return $this->createFile(
            'contract',
            [
                'namespace' => 'App\\Domain\\Contracts',
                'model'     => $model,
                'fields'    => $fields,
                'fillables' => $fillables,
            ],
            app_path() . '/Domain/Contracts',
            $model . 'Contract.php'
        );
    }

    public function createModel($name, $fillables, $softDeletes, $pageLength, $fields)
    {
        return $this->createFile(
            'model',
            [
                'namespace'   => 'App\\Models',
                'model'       => $name,
                'fillables'   => $fillables,
                'fields'      => $fields,
                'softDeletes' => $softDeletes,
                'pageLength'  => $pageLength,
            ],
            app_path() . '/Models',
            $name . '.php'
        );
    }

    public function createMigration($name, $entity, $softDeletes)
    {
        return $this->createFile(
            'migration',
            [
                'model'       => $name,
                'fields'      => $entity,
                'softDeletes' => $softDeletes,
            ],
            database_path() . '/migrations',
            date('Y_m_d_His') . '_create_' . Str::plural(
                Str::lower($name)
            ) . '_table.php'
        );
    }

    public function createFactory($name, $entity)
    {
        return $this->createFile(
            'factory',
            [
                'model'  => $name,
                'fields' => $entity,
            ],
            database_path() . '/factories',
            $name . 'Factory.php'
        );
    }

    public function createCreateRequest($name, $fields, $params)
    {
        return $this->createFile(
            'request',
            [
                'model'  => $name,
                'fields' => $fields,
                'params' => $params,
                'type'   => 'Create',
            ],
            app_path() . '/Http/Requests',
            'Create' . $name . 'Request.php'
        );
    }

    public function createAdminPages($name, $entity)
    {
        return $this->createFile(
            'admin-entity-index-page',
            [
                'name'   => Str::snake($name),
                'entity' => $entity,
            ],
            public_path() . '/admin/pages/' . Str::lower(Str::kebab($name)),
            'index.vue',
            false
        );
    }

    public function createNuxtConfig()
    {
        return $this->createFile(
            'nuxt-config',
            [],
            public_path() . '/admin',
            'nuxt.config.js',
            false
        );
    }

    public function createItemsState($entities)
    {
        return $this->createFile(
            'navbar',
            [
                'entities' => $entities,
            ],
            public_path() . '/admin/store',
            'navbar.js',
            false
        );
    }

    public function createUpdateRequest($name, $fields, $params)
    {
        return $this->createFile(
            'request',
            [
                'model'  => $name,
                'fields' => $fields,
                'params' => $params,
                'type'   => 'Update',
            ],
            app_path() . '/Http/Requests',
            'Update' . $name . 'Request.php'
        );
    }

    public function createGetRequest($name, $fields, $params)
    {
        return $this->createFile(
            'request',
            [
                'model'  => $name,
                'fields' => $fields,
                'params' => $params,
                'type'   => 'Get',
            ],
            app_path() . '/Http/Requests',
            'Get' . $name . 'Request.php'
        );
    }

    public function createController($model)
    {
        return $this->createFile(
            'controller',
            [
                'namespace' => 'App\\Http\\Controllers\\API\\V1',
                'model'     => $model,
            ],
            app_path() . '/Http/Controllers/API/V1',
            class_basename($model) . 'Controller.php'
        );
    }

    public function createService($model)
    {
        return $this->createFile(
            'service',
            [
                'namespace' => 'App\\Services',
                'model'     => $model,
            ],
            app_path() . '/Services',
            $model . 'Service.php'
        );
    }

    public function createRepository($model)
    {
        return $this->createFile(
            'repository',
            [
                'namespace' => 'App\\Domain\\Repositories',
                'model'     => $model,
            ],
            app_path() . '/Domain/Repositories',
            $model . 'Repository.php'
        );
    }

    public function createRouteServiceProvider($entities)
    {
        return $this->createFile(
            'route-service-provider',
            [
                'entities' => $entities,
            ],
            app_path() . '/Providers',
            'RouteServiceProvider.php'
        );
    }

    public function createDatabaseSeeder($entities)
    {
        return $this->createFile(
            'database-seeder',
            [
                'entities' => $entities,
            ],
            database_path() . '/seeders',
            'DatabaseSeeder.php'
        );
    }

    public function createAdminAuthRoutes()
    {
        return $this->createFile(
            'auth-routes',
            [],
            base_path() . '/routes/api/v1/admin',
            'auth.php'
        );
    }

    public function createRoutes($entity)
    {
        return $this->createFile(
            'routes',
            [
                'entity' => $entity,
            ],
            base_path() . '/routes/api/v1',
            Str::lower(Str::kebab($entity)) . '.php'
        );
    }

    /**
     * @param string $view
     * @param array  $data
     * @param string $path
     * @param string $name
     * @param bool   $php
     *
     * @return false|int
     */
    private function createFile(string $view, array $data, string $path, string $name, bool $php = true)
    {
        $file = view(
            'template::' . $view,
            $data
        )->render();

        if ($php) {
            $file = "<?php\n\n" . $file;
        }

        if (!file_exists($path)) {
            if (!mkdir($concurrentDirectory = $path, 0777, true) && !is_dir(
                    $concurrentDirectory
                )) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }
        }

        return file_put_contents($path . '/' . $name, $file);
    }
}
