<?php

namespace Japananimetime\Template\Providers;

use Illuminate\Support\ServiceProvider;
use Japananimetime\Template\Console\Commands\MakeEntity;

class TemplateServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadViewsFrom(__DIR__ . '/../../resources/views/templates', 'template');
        if ($this->app->runningInConsole()) {
            $this->commands(
                [
                    MakeEntity::class,
                ]
            );
        }
        $this->publishes(
            [
                __DIR__ . '/../../public/admin' => public_path('admin'),
            ], 'public'
        );
    }
}
